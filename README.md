## Author info
Hugues Rouillard 2242264
## Assignment
containerize one of our JS applications from the 320 course in 2023
## Application
Guessing game. Choose a color to guess upon, generate a table, and guess which tiles have the highest rgb value for the selected color.
## Instruction
 To create dockerfile:
    1 specify base image with FROM
    2 enter user defined metadata with LABEL
    3 execute on a new layer then commited to image with RUN
    4 set working directory on container with WORKDIR
    5 copy files from a defined source(s) to container's file system
    6 specify ports to be published by container app with EXPOSE
    7 specify what container should run when launched with ENTRYPOINT

