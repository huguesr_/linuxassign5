'use strict';
  /**
  * @title createTable
  * @description Creates the table of color for the game 
  * @param {event} e
  * @author Vijay Patel
  */
function createTable(e){
    const row = document.querySelector('#board-size').value;
    let colorTable = document.querySelector("#color-Table");
    let table = document.createElement("table");
    let difficulty = checkDifficulty(e);
    changeBackgroudColor(difficulty);

    colorTable.textContent = undefined;

    for(let i = 0;i<row;i++){
        let tr = document.createElement("tr");
        for(let j = 0;j<row;j++){
        createCell(difficulty, tr);
        }
        table.appendChild(tr);
    }
    colorTable.appendChild(table);

    addSubmitButton();
    disableForm();
}
  /**
  * @title addSubmitButton
  * @description Add a submit button in order to submit the answers for the score
  * @author Hugues Rouillard
  */
function addSubmitButton() {
    document.querySelector('#num-answers-and-guesses').innerText = "Start by selecting a tile!";

    let submitButton = document.createElement("button");
    let submitDiv = document.querySelector("#submit-button-div");
    submitDiv.textContent = undefined;
    submitButton.setAttribute("id", "submit-button");
    submitButton.setAttribute("type", "submit");
    submitButton.innerText = "Submit Guess!";
    
    submitButton.addEventListener("click", function(e) {
        const playerName = document.querySelector('#player-name').value;
        let colorChosen = document.querySelector("#color").value;
        alertScore(colorChosen);
        activateForm();
        addLocalStorage(playerName,score());
        insertScoreTable();
        
        submitButton.disabled = true;
        //this removes the event listners from all the td's
        let td = Array.from(document.querySelectorAll("#color-Table table tr td"));
        td.forEach((x) =>{
            x.removeEventListener("click",selectTiles,{ capture: true });
            }
        )
    });

    submitDiv.appendChild(submitButton);
}
  /**
  * @title createCell
  * @description Creates the individual cells(td) for the gameBoard
  * @param {number} difficulty
  * @param {HTMLElement} tr
  * @author Hugues Rouillard
  */
function createCell(difficulty, tr) {
    let td = document.createElement("td");
  
    td.classList.add("unselected");
    let color = document.querySelector("#color").value;
    td.addEventListener("click",selectTiles,{ capture: true });
  
    let div = document.createElement("div");
    let div2 = document.createElement("div");
    div.classList.add("rgbanswers");
    div2.classList.add("coloranswers");
    div.textContent = colorSet(color, td, difficulty)[0];
    div2.textContent = colorSet(color, td, difficulty)[1];
  
    td.appendChild(div);
    td.appendChild(div2);
  
    tr.appendChild(td);
}
  /**
  * @title disableForm
  * @description Disable the form when you start the game
  * @author Vijay Patel
  */
function disableForm() {
    let form = document.querySelector("#form-inputs");
    let formInputs = form.querySelectorAll("div");

    formInputs.forEach(element => {
        let input = element.querySelector('input');
        let select = element.querySelector('select');
        if(input){
        input.disabled = true;
        }
        if(select){
        select.disabled = true;
        }
        document.getElementById("start-game-button").disabled = true;
    });
}

  /**
  * @title activateForm
  * @description Reenable the form when you submit your answer
  * @author Vijay Patel
  */
function activateForm() {
    console.log("Activating awsomeness");
    let form = document.querySelector("#form-inputs");
    let formInputs = form.querySelectorAll("div");
  
    formInputs.forEach(element => {
      let input = element.querySelector('input');
      let select = element.querySelector('select');
      if (input) {
        input.disabled = false;
      }
      if (select) {
        select.disabled = false;
      }
      document.getElementById("start-game-button").disabled = false;
    });
}
  
