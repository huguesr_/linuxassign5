"use strict";

  /**
  * @description A function that changes the title of the page when you leave the tab
  * @author Vijay Patel 
  */
  document.addEventListener("visibilitychange",function(){
    let title = document.querySelector("title");
    if(document.visibilityState !== 'visible'){
      title.textContent = "😭 come back <3";
    } else {
      title.textContent = "H&V Project";
    }
  });