"use strict";
  /**
  * @title selectTiles
  * @param {String} colorChosen
  * @description Counts the number of tiles for the selected color
  * @author Hugues Rouillard
  */
function correctAnswersCount(colorChosen){
  let tdValues = Array.from(document.querySelectorAll(".coloranswers"));
  let numColor = 0;
  tdValues.forEach((x) => {
    if(x.textContent === colorChosen){
      numColor++;
    }
  });
  return numColor;
}
  /**
  * @title selectTiles
  * @param {event} e
  * @description this changes the class of a tile which would make its border yellow if selected and gray if unselected
  * @author Vijay Patel
  */
function selectTiles(e) {
  let color = document.querySelector("#color").value;
  let closest = e.target.closest("td").classList;
  if (closest.contains("unselected")) {
    e.currentTarget.classList.remove("unselected");
    e.currentTarget.classList.add("selected");
  } else if (closest.contains("selected")) {
    e.currentTarget.classList.remove("selected");
    e.currentTarget.classList.add("unselected");
  }
  bottomText(correctAnswersCount(color), color);
}

  /**
  * @title bottomText
  * @param {number} correctNum
  * @param {String} color
  * @description Prints the bottom text to show how many selected tiles
  * @author Hugues Rouillard 
  */
function bottomText(correctNum, color){
  const bottomTextDiv = document.querySelector('#num-answers-and-guesses');

  let numSelected = numberSelected();
  bottomTextDiv.innerText = `Searching for ${color}! Your target is: ${correctNum}! ${numSelected} tile selected.`
}

  /**
  * @title numberSelected
  * @description Returns the amount of tiles selected
  * @author Hugues Rouillard 
  * @return {number}
  */
function numberSelected() {
  let tdSelected = Array.from(document.querySelectorAll('.selected'));
  let numSelected = tdSelected.length;
  return numSelected;
}

  /**
  * @title checkAnswers
  * @description Returns which of the selected squares are correct
  * @param {String} color
  * @author Hugues Rouillard 
  * @return {number}
  */
function checkAnswers(color){
  let selectedAnswers = Array.from(document.querySelectorAll('.selected .coloranswers'));
  let rightAnswers = 0;
    for (let i=0; i<selectedAnswers.length; i++){
      if(selectedAnswers[i].textContent === color){
        rightAnswers++;
      }
    }
    return rightAnswers;
}