'use strict';
/**
 * the JavaScript behind the web dev project
 * @author Vijay Patel & Hugues Rouillard
 */
/**
 * @title DomContentLoaded
 * @description Loads the Dom
 * @author Vijay Patel & Hugues Rouillard
 */
document.addEventListener("DOMContentLoaded", function(){
  insertScoreTable();
  /**
  * @description Adds an event listener on the form whic would create a table inside the needed area
  * @author Vijay Patel & Hugues Rouillard
  */
  document.querySelector("#setup-form").addEventListener("submit",function(e){ 
    e.preventDefault();
    createTable(e);
  });
  /**
  * @description Adds an event listener on the button which serves to clear the board
  * @author Hugues Rouillard
  */
  document.querySelector('#clear').addEventListener('click', clearBoard);
});
