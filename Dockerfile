#BUILD docker build -t huguesassign5
#RUN docker run -d -p 8885:80 huguesassign5

FROM httpd

LABEL maintainer="Hugues" email="hugues.rouillard@dawsoncollege.qc.ca" modified="2023-05-24"

#DocumentRoot for httpd
WORKDIR /usr/local/apache2/htdocs/

#Install app from host to container
COPY ./app .

